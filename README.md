# General Stuff:

---

## Gradle

### Gradle Daemon

O Gradle corre na JVM, usando um conjunto de librarias que requerem um grande tempo de inicialização.
Para evitar o recarregar destas mesmas librarias a cada build, recorre-se a um Gradle Daemon, um processo em background que corre continuamente.\
O Gradle usa Gradle Daemons por default desde a versão 3.0.
Estes têm impacto significativo na performance das builds, uma vez que há muita informação que é guardada em memória entre builds.

Pode ser desativado, por exemplo, através do comando `org.gradle.daemon=false` no ficheiro `.gradle/gradle.properties`.

### Gradle Wrapper

Gradle Wrapper é um script que invoca uma versão específica do Gradle, de maneira a que haja consistência entre builds, fazendo download dessa mesma versão previamente, se necessário.\
Se a versão especificada já estiver instalada, o Wrapper servirá de proxy para a mesma.

Isto previne que haja problemas derivados de novas versões do Gradle.

Para incluir o Gradle Wrapper num projeto, usa-se o comando `$ gradle wrapper --gradle-version 2.4` (neste caso em concreto, a versão definida é a 2.4).

### build.gradle

A chamada de custom tasks é feita através do comando `$ gradle taskName`. Adicionalmente, este comando executa a task especificada para todos os subprojects. \
A chamada de custom tasks de um subproject específico é feita através do comando `$ gradle :mySubproject:taskName`.

A definição de uma task dá-se da seguinte maneira:

```
task someTask {
  println("hello, I'm always run, whatever the task you choose to execute")
  // here's the code that should be executed if the user chooses to execute this task
  doLast {
    println("hello, I'm the code that is only executed if the user chooses to execute this task")
  }
}
```

As tasks podem ser também podem ser criadas dinamicamente e as respetivas dependências podem ser definidas em runtime, um exemplo disto será:

```
4.times { counter ->
    task "task$counter" {
        doLast {
            println "I'm task number $counter"
        }
    }
}
task0.dependsOn task2, task3
```

#### Repositories

```
repositories {
    // Usage of Maven central repository
    mavenCentral()

    // Usage of a remote Maven repository
    maven {
        url "http://repo.mycompany.com/maven2"
    }

    // Usage of a local Ivy directory.
    ivy {
        url "../local-repo"
    }
}

```

#### Java Plugin

`apply plugin: 'java'`

Main flow:

![Java_Plugin_Tasks](./images/Screenshot_2.png)

### Build Lifecycle

Uma build do Gradle tem 3 fases:
- **Initialization**: Durante esta fase, o Gradle determina quais são os projetos (o Gradle suporta builds multi-projeto) que vão participar no processo de build, criando para cada um deles uma instância de **Project**;
- **Configuration**: Durante esta fase, os objetos do projeto são configurados. Os scripts de build de todos os projetos que fazem parte da build são executados;
- **Execution**: O Gradle determina o sub-conjunto de tasks, criadas e configuradas durante a fase de configuração, a serem executadas. Este sub-conjunto é determinado através dos nomes das tasks passados como argumento aquando da chamada do Gradle (tendo em consideração o diretório atual) - a ordem pela qual os nomes são passados como parâmetro é tida em consideração.

#### Exemplo

**settings.gradle**
`println 'This is executed during the initialization phase.'`

**build.gradle**
```
println 'This is executed during the configuration phase.'

task configured {
    println 'This is also executed during the configuration phase.'
}

task test {
    doLast {
        println 'This is executed during the execution phase.'
    }
}

task testBoth {
	doFirst {
	  println 'This is executed first during the execution phase.'
	}
	doLast {
	  println 'This is executed last during the execution phase.'
	}
	println 'This is executed during the configuration phase as well.'
}
```

Quando se executa o comando `gradle test testBoth`, o output será:
```
> gradle test testBoth
This is executed during the initialization phase.

> Configure project :
This is executed during the configuration phase.
This is also executed during the configuration phase.
This is executed during the configuration phase as well.

> Task :test
This is executed during the execution phase.

> Task :testBoth
This is executed first during the execution phase.
This is executed last during the execution phase.

BUILD SUCCESSFUL in 0s
2 actionable tasks: 2 executed
```

### Task Dependencies and Task Ordering

**Task Ordering** - uma regra que afete as Tasks A e B - `A mustRunAfter B` - só tem efeito se ambas as tasks forem invocadas.

**Task Dependency** - uma regra que afete as Tasks A e B - `A dependsOn B` - tem efeito sempre que a task A é invocada. Neste caso concreto, a task B é executada mesmo sem ser invocada.  

---

## Groovy

Groovy é a linguagem usada pelo Gradle.

*Closures* são blocos de código entre '{}' que podem receber parâmetros, executar tarefas e retornar valores, podendo ser invocados, quer através de invocação direta pelo nome, quer pela utilização da função `call()`.

![Groovy_Closures](./images/Screenshot_1.png)

### Custom Groovy Tasks

A criação de uma *Custom Groovy Task* precisa de um ficheiro com extensão `.groovy`.\
Estes ficheiro ficarão guardados na `buildSrc/src/main/groovy/`.

**exemplo.groovy**
```
package com.example;

import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

class GreetingTask extends DefaultTask {
    @Input
    String greeting = 'hello from GreetingTask'

    @TaskAction
    def greet() {
        println greeting
    }
}

```

A inclusão da nova task no `build.gradle`, dar-se-ia da seguinte forma:
```
// Use the default greeting
task hello(type: com.example.GreetingTask)
```

Utilizando como **Task Type** o nome da classe especificado no `exemplo.groovy` e o seu **path** a partir do diretório base para os ficheiros desta natureza.

No caso concreto apresentado, seria ainda possível personalizar a mensagem apresentada, através da seguinte task no `build.gradle`:
```
// Customize the greeting
task greeting(type: GreetingTask) {
    greeting = 'greetings from GreetingTask'
}
```

A chamada das duas tasks criadas acima daria o seguinte output:
```
> gradle -q hello greeting
hello from GreetingTask
greetings from GreetingTask
```

---

## Continuous Deployment & Continuous Delivery

**Continuous Deployment**: todas as alterações são automaticamente implantadas (deployed) para produção.

**Continuous Delivery**: a equipa garante que todas as mudanças estão prontas para ser implantadas (deployed) para produção, mas poderá optar por não o fazer, normalmente por razões associadas à parte do negócio.

Baseia-se na utilização do padrão denominado **Deployment Pipeline**.

Ambos devem ser processos automatizados e de ciclos curtos (releases frequentes), estando dependentes de **feedback**.

### Deployment Pipeline

![Deployment_Pipeline_01](./images/Screenshot_3.png)

Para cada alteração feita à configuração, código fonte, dados ou ambiente de um aplicação é criada uma nova instância da pipeline.

O primeiro passo da pipeline é a criação de binários e instaladores, que serão usados pelos restantes passos da pipeline para a execução de testes, de modo validar os tais binários (**candidatos a release**) para release.

Se um determinado candidato a release passar os testes todos, está pronto para release.

A **Deployment Pipeline** tem origem nos processos de **Continuous Integration**.

![Deployment_Pipeline_02](./images/Screenshot_4.png)

![Deployment_Pipeline_03](./images/Screenshot_5.png)

### Feedback

De modo a ser útil, o **feedback** deve seguir três critérios:

- Toda e qualquer mudança* deve ativar um processo de feedback. Dependendo da vertente alterada, estamos perante casos de:
  - **Continuous Integration**;
  - **Configuration Management**;
  - **Infrastructure and Environment Management**.
- Feedback deve ser entregue o mais prontamente possível;
- A equipa de delivery deve receber o feedback e atuar consoante o mesmo. Esta incorpora:
  - Developers;
  - Testers;
  - Operations Staff;
  - Database Administrators;
  - Infrastructure Specialists;
  - Managers.

\* software pode ser composto em quatro vertentes:
  - Código executável;
  - Configuração;
  - Host Environment;
  - Dados.

### Continuous Integration

Código executável - binários - muda quando se verifica uma alteração no código fonte. Estes devem ser gerados novamente.

A prática de dar build e testar a aplicação a cada uma destas alterações, de forma automática, é denominada de **continuous integration**.

### Configuration Management

Diferenças entre ambientes de execução devem ser registadas como informações de configuração. Qualquer tipo de alteração a estas configurações deve ser testado.\
Se o software é para ser instalado por utilizadores, é recomendado que as configurações possíveis para os ambientes de execução sejam bastante abrangentes.

### Infrastructure and Environment Management

Se o ambiente para o qual a aplicação é deployed sofre alterações, todo o sistema deve ser testado com essas alterações em prática.\
Aqui incluem-se alterações a nível de configuração do OS, network, infraestruturas, entre outros.

Da mesma forma, alterações à estrutura de dados deverão ser testadas.

### Continuous ...?

**Continuous Deployment** e **Continuous Delivery** são ambos uma extensão de **Continuous Integration**.

|Continuous|Automated Build|Automated Testing|Manual Prompt|Automated Release|
|:--:|:--:|:--:|:--:|:--:|
|Integration|x|x|||
|Delivery|x|x|x|x|
|Deployment|x|x||x|

![Continuous](./images/Screenshot_13.png)

---

## Testing

Tipos de testes:

![Types of Tests](./images/Screenshot_14.png)

### Non-Functional Testing

#### Non-Functional Requirements

**Performance**: Medida de tempo que um processo demora a executar uma determinada transação. Pode ser medido isoladamente ou em carga.

**Throughput** (Rendimento): A quantidade de transações que o sistema consegue processar num determinado período. Esta será sempre limitada por um bottleneck do sistema.

**Capacity**: A capacidade de um sistema suportar o **Rendimento** máximo enquanto mantém um tempo de resposta razoável para cada um dos requests feitos.

Requisitos não-funcionais têm tendência a não serem compatíveis entre si. Por exemplo - e de um modo abstrato e geral - **Segurança** costuma sacrificar **Usabilidade**, assim como **Flexibilidade** costuma sacrificar **Performance**.\
A 'pesagem' da importância dos requisitos é feita através do uso do **ATAM** :)

#### JMeter

![JMeter](./images/Screenshot_6.png)

### Testing Doubles

Um **Test Double** é um termo genérico aplicado a uma instância em que se substitui um objeto de produção para efeitos de teste.

3 exemplos são:
 - **Dummy**, que são objetos que são passados mas nunca são realmente usados. Normalmente o seu único uso é preencher listas de parâmetros.
 - **Mocks**, que são objetos pré-programados com *expectations* para as chamadas que estão à espera de receber - estes criam uma exceção se:
  - Recebem uma chamada que não estão à espera;
  - Não recebem uma chamada que estão à espera;
  - Recebem as chamadas que estão à espera, mas por ordem diferente.
 - **Stub**, que são objetos que contém dados pré-definidos usados para responder a chamadas durante a fase de testes.

---

## Cucumber

Cucumber usa a DSL Gherkin para escrever as suas especificações, em ficheiros `.feature`. O Gherkin permite delinear testes automáticos sem entrar em pormenores de implementação.

Através da utilização de cenários (`.feature`), o Cucumber permite um *Behaviour-Driven Development*, onde estes são escritos **antes** do código de produção.

Como exemplo:

```
# -- FILE: features/gherkin.rule_example.feature
Feature: Highlander

  Rule: There can be only One

    Example: Only One -- More than one alive
      Given there are 3 ninjas
      And there are more than one ninja alive
      When 2 ninjas meet, they will fight
      Then one ninja dies (but not me)
      And there is one ninja less alive

    Example: Only One -- One alive
      Given there is only 1 ninja alive
      Then he (or she) will live forever ;-)

  Rule: There can be Two (in some cases)

    Example: Two -- Dead and Reborn as Phoenix
      ...
```

A syntax do Cucumber funciona por indentação e rege-se por keywords.

### Keywords/Secções de relevância

#### Feature:

Fornece uma descrição de alto-nível de uma funcionalidade do software em causa. Usada para agrupar cenários relacionados. Deve ser sempre a primeira keyword de um ficheiro Gherkin, seguida de um texto que descreve a funcionalidade.

```
Feature: Guess the word

  The word guess game is a turn-based game for two players.
  The Maker makes a word for the Breaker to guess. The game
  is over when the Breaker guesses the Maker's word.
```

O restante texto não é relevante para a execuçáo do Cucumber, mas é importante para questões de documentação:
- O primeiro texto é o nome da *feature*;
- O segundo texto é uma descrição da *feature*.

#### Example/Scenario:

São sinónimos e representam uma regra de negócio, consistindo numa lista de **steps**.

Não há limite para o número de **steps** num determinado **Example**/**Scenario**, mas devem ser um número reduzido (3-5).

#### Steps:

Cada step começa com uma das seguintes keywords: **Given**, **When**, **Then**, **And** ou **But**.\
Estes são executados sequencialmente.

Diferentes steps não podem partilhar o mesmo texto descritivo, como no exemplo abaixo:

```
Given there is money in my account
Then there is money in my account
```

##### Given

Este step define um contexto inicial do sistema, normalmente, descrevendo algo que já aconteceu (e.g.: *The user loggen in*). Visa meter o sistema num estado conhecido e bem definido.

Num contexto de criação de *Casos de Uso*, o **Given** seriam as *pré-condições*.

Usando as keywords `And` e `But`, é possível haver mais que um **Given**.

##### When

Este step é usado para descrever um evento ou uma ação - desde uma interação de um utilizador com o sistema ou entre sistemas.

Apesar de ser possível ter mais que um **When**, a presença de mais que um é, geralmente, indicativo que o scenario poderá ser dividido em mais scenarios.

##### Then

Os steps **Then** servem para descrever o resultado espectável de um scenario, usando um *assert*.\
O resultado deverá ser observável, ou seja, deverá evitar-se implementar **Then**'s que se sirvam da base de dados para obter resultados (isto deveria de ser evitado de qualquer maneira, uma vez que a utilização do Cucumber não deve ter qualquer tipo de implementação em conta) - estes devem contar com mensagens/sinais visíveis ao utilizador.

##### And & But

Embora não obrigatórias, as keywords **And** e **But** são utilizadas para aumentar a *readability* do código (as keywords em si não são utilizadas em execução pelo Cucumber, servindo de guia visual para o programador).

Ou seja, em casos como o apresentado abaixo:

```
Example: Multiple Givens
  Given one thing
  Given another thing
  Given yet another thing
  When I open my eyes
  Then I should see something
  Then I shouldn't see something else
```

Poder-se-á usar:

```
Example: Multiple Givens
  Given one thing
  And another thing
  And yet another thing
  When I open my eyes
  Then I should see something
  But I shouldn't see something else
```


O tipo de expressão:
```
Given X
When Y
Then Z
```
é tambem designado por **Happy Path**.

---

## Blue/Green Deployment

Quando se tem mais que um ambiente de Produção, é possível manter um deles inativo, enquanto o outro está ativo.\
O ambiente inativo é designado de **Blue**, o ativo é o **Green**.

### Fluxo Principal

O deployment da nova aplicação é feita para o ambiente **Blue** e todos os testes são corridos neste ambiente. Caso todos os testes passem, o **Blue** passa a estar ativo, enquanto que o **Green** é desativado. Trocam, então, designação entre si.

### Bases de Dados separadas

Após o deployment do ambiente **Blue**, quando existem alterações ao *schema* da base de dados ou à base de dados em si, mete-se a base de dados **Green** em modo de *read-only* e cria-se uma cópia, passando-a para o ambiente **Blue**.\
O tráfego é redirecionado para o ambiente **Blue**, metendo-se a sua base de dados em modo *read-write*.

Caso haja uma falha e haja novos dados inseridos, são remetidos para a base de dados antiga.

### Shadow Environment Releasing

Também conhecido como **Live-Live Releasing**, é uma versão do **Blue/Green Deployment** que utiliza os ambientes de **Staging** e **Production**, em vez de haverem 2+ ambientes de Produção.

---

## Nível de Maturidade

Existem 5 níveis de maturidade:
 - **Level 3 - Optimizing**: Há um foco na otimização do processo;
 - **Level 2 - Quantitatively Managed**: Existem métricas e controlo sobre os processos;
 - **Level 1 - Consistent**: Processos são automatizados. Esta automatização estende-se ao longo de todo o lifecycle da aplicação;
 - **Level 0 - Repeatable**: Processo está documentado e parcialmente automatizado;
 - **Level -1 - Regressive**: O processo não é repetível, há pouco controlo e é reactivo.

### Build Management and Continuous Integration

![Maturity_01](./images/Screenshot_7.png)

### Environments and Deployment

![Maturity_02](./images/Screenshot_8.png)

### Release Management and Compliance

![Maturity_03](./images/Screenshot_9.png)

### Testing

![Maturity_04](./images/Screenshot_10.png)

### Data Management

![Maturity_05](./images/Screenshot_11.png)

### Configuration Management

![Maturity_06](./images/Screenshot_12.png)

---

# Resoluções:

## N_18_19

### Parte 1

- 1 - F
- 2 - V
- 3 - F
- 4 - V
- 5 - V
- 6 - F (JMeter é uma ferramenta para testes não-funcionais, nomeadamente relacionados com performance)
- 7 - F
- 8 - F (ao contrário..?)
- 9 - F (é possível fazer isso, mas não é o objetivo principal)
- 10 - V (Gherkin é a linguagem do Cucumber)
- 11 - F (o contrário é considerado um anti-pattern)
- 12 - V
- 13 - F
- 14 - F
- 15 - V

### Parte 2

Questão 2

`A flag -b serve para especificar qual o ficheiro a ser usado.`

Output 1: 0 1 4 5
Output 2: 0 1 4 5 3 2
Output 3: 0 5 1 4 3 2

- 1 - 0 vezes
- 2 - 0 vezes
- 3 - 3 vezes
- 4 - 2 vezes
- 5 - 1 vez

Questão 3

 - 1 - `getMap().put(c.getName(), c);`
 - 2 - `assert ContactMap.getSize() == 1;` (assert é uma keyword do Java)
 - 3 - `ContactMap.put(new Contact("(.*?)"));`
 - 4 - `ContactMap.reset();`

### Parte 3

Questão 4

- 1 - `post`
- 2 - `agent`
- 3 - `stages`
- 4 - `Code Quality`
- 5 - `paralel`
- 6 - `Tests`
- 7 - `success`

Questão 5

- 1 - master, develop
- 2 - hotfix
- 3 - (`git checkout feature_2`, `git commit`,)? `git branch release_2`, `git checkout release_2`, `git merge feature_2`
- 4 - `git checkout feature_1`, `git commit`, `git checkout develop`, `git merge feature_1`, `git commit`, `git checkout release_1`, `git merge develop`, `git commit`, `git checkout master`, `git merge release_1`

---

## R_18_19

### Parte 1

- 1 - V
- 2 - F
- 3 - V
- 4 - F
- 5 - V
- 6 - F
- 7 - V
- 8 - V
- 9 - F
- 10 - F
- 11 - V
- 12 - V
- 13 - V
- 14 - V
- 15 - V

### Parte 2

Questão 2

Output 1: 1 0 5 4 6
Output 2: 1 0 5 4   
Output 3: 1 0 5 4 3 2 6

 - 1 - 2 vezes
 - 2 - 0 vez
 - 3 - 2 vezes, o '5' é na fase de configuração e o '6' na de execução, mas o '6' só vai ser impresso duas vezes.
 - 4 - 1 vez, na terceira execução, porque `task0.mustRunAfter odsoft`
 - 5 - 3 vezes

Questão 3

 - 1 - finishTest();
 - 2 - ContactsService.class
 - 3 - delayTestFinish(10000);
 - 4 - GWTTestCase
 - 5 - ContactsService.class
 - 6 - Contact

Questão 4

 - 1 -
 - 2 - `sh 'gradle build'`
 - 3 - `**.*war`
 - 4 - `sh 'gradlew test'`
 - 5 - `Test`

Questão 5

 - 1 - Continuous Delivery
 - 2 - mais rápidos
 - 3 - agent
 - 4 - automatizado
 - 5 - Jenkinsfile

---

## N_17_18

### Parte 1

 - 1 - F v
 - 2 - V v
 - 3 - V v
 - 4 - F v
 - 5 - F v
 - 6 - F v
 - 7 - V v
 - 8 - F x
 - 9 - V v
 - 10 - F v
 - 11 - F v, é o nome default, mas pode assumir outros
 - 12 - F v
 - 13 - F v
 - 14 - V v
 - 15 - V v
 - 16 - V v
 - 17 - V v
 - 18 - V v
 - 19 - ?
 - 20 - F f

#### Pergunta 2

 - 1 - 4
 - 2 - 2
 - 3 - 3
 - 4 - 5
 - 5 - 1

### Pergunta 3

 - 1 - `type: com.example.CheckWebsite`
 - 2 - `(type: com.example.CheckWebsite) { url = 'www.google.com' }`
 - 3 - `src/main/groovy/com/example/CheckWebsite.groovy`
 - 4 - `buildSrc/`

## Parte 2

### Pergunta 4

 - 1 - 8
 - 2 - 2
 - 3 - 6
 - 4 - 16

### Pergunta 5

 - 1 - `./build/libs/cms-1.0.war`
 - 2 - `EXPOSE`
 - 3 - `tomcat`

### Pergunta 6

 - 1 - `fruitPrice.put(aFruit, aPrice);`
 - 2 - `"banana"`
 - 3 - `String aFruit, int aPrice`

### Pergunta 7

 - 1 - `8080`
 - 2 - `localhost:4040/cmsapp`
 - 3 - `cmsapp_container2`
 - 4 - `container`
 - 5 - `sleep 5`
